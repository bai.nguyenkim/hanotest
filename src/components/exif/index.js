import {
    Platform,
    NativeModules,
} from 'react-native';

const Exif = {};

Exif.cloneImage = function (uri, position) {
    const path = uri.replace('file://', '');
    return NativeModules.ReactNativeExif.cloneImage(path, position).then(result => {
        return result;
    }).catch(console.log);
};

Exif.cloneWithCurrentLocation = function (uri, position) {
    const path = uri.replace('file://', '');
    return NativeModules.ReactNativeExif.cloneWithCurrentLocation(path, position).then(result => {
        return result;
    }).catch(console.log);
};

Exif.getMetaData = function (uri) {
    const path = uri.replace('file://', '');
    return NativeModules.ReactNativeExif.getMetaData(path).then(result => {
        return result;
    }).catch(console.log);
};

module.exports = Exif;
