
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {useEffect, useState} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    FlatList,
    View,
    Text,
    ScrollView, Image, Dimensions, TouchableOpacity, RefreshControl
} from 'react-native';

import {
    Colors,
} from 'react-native/Libraries/NewAppScreen';
import Exif from "../../components/exif/index";
import {useNavigation} from "react-navigation-hooks";
import Geolocation from 'react-native-geolocation-service';
import Permissions from 'react-native-permissions';

const Detail = () => {

    const { goBack, getParam } = useNavigation();
    const [ info, setInfo ] = useState({});
    const item = getParam('item', {});
    const onComplete = getParam('onComplete', () => {});


    useEffect(() => {
        Exif.getMetaData(item.uri).then(result => {
            setInfo(result)
        })
    }, []);

    const updateLocation = () => {
        Permissions.request('location').then(response => {
            if (response === 'authorized') {
                Geolocation.getCurrentPosition(position => {
                        console.log(position);
                        Exif.cloneImage(item.uri, position).then(() => {
                            onComplete()
                            goBack()
                        })
                    }, error => {
                        console.log(error.code, error.message);
                    },
                    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
                );
            }
        });
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => {
                    goBack()
                }}>
                    <Text>Back</Text>
                </TouchableOpacity>
                <Text style={styles.title}>
                    Detail
                </Text>
            </View>
            <Image style={styles.image} source={{ uri: item.uri }}/>
            <View style={{ alignItems: 'center' }}>
                <TouchableOpacity
                    style={{
                        backgroundColor: '#6060f0',
                        paddingHorizontal: 8,
                        paddingVertical: 4,
                        borderRadius: 4
                    }}
                    onPress={() => {
                        updateLocation()
                    }}>
                    <Text style={{ color: 'white' }}>Clone with current location</Text>
                </TouchableOpacity>
            </View>
            <ScrollView style={styles.scrollView} contentContainerStyle={styles.contentScroll}>
                <Text>{JSON.stringify(info, null, 4)}</Text>
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
      paddingHorizontal: 16,
      paddingVertical: 8,
      alignItems: 'center',
        flexDirection: 'row'
    },
    title: {
        flex: 1,
        textAlign: 'center'
    },
    scrollView: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentScroll: {
        paddingVertical: 16
    },
    image: {
        resizeMode: 'contain',
        flex: 1,
        backgroundColor: Colors.lighter
    }

});

export default Detail;
