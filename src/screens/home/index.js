
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {useEffect, useState} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    FlatList,
    View,
    Text,
    StatusBar, Image, Dimensions, TouchableOpacity, RefreshControl
} from 'react-native';

import {
    Colors,
} from 'react-native/Libraries/NewAppScreen';

import CameraRoll from "@react-native-community/cameraroll";
import Permissions from 'react-native-permissions';
import * as moment from 'moment'
import Exif from "../../components/exif/index";
import {isEmpty} from "lodash";
import { useNavigation } from 'react-navigation-hooks'


const Home = () => {
    const [state, setState] = useState({
        photoPermission: 'denied'
    });
    const [images, setImages] = useState([]);
    const [pageInfo, setPageInfo] = useState({});
    const [refreshing, setRefreshing] = useState(false);
    const [imageInfo, setImageInfo] = useState({});
    const { navigate } = useNavigation();

    useEffect(() => {
        requestPermission()
    }, []);


    const requestPermission = () => {
        Permissions.request('photo').then(response => {
            setState({photoPermission: response});
            if (response === 'authorized') {
                getImagesFromLibrary(true)
            }
        });
    }

    const getImagesFromLibrary = (fresh) => {
        // if (state.photoPermission !== 'authorized') return

        CameraRoll.getPhotos({
            first: 21,
            after: !fresh ? pageInfo.end_cursor : undefined,
            assetType: 'Photos'
        }).then(({edges, page_info}) => {
            setRefreshing(false)
            console.log(edges)
            if (fresh) {
                console.log('clear')
                setImages(edges)
            } else {
                console.log('concat')
                setImages(images.concat(edges))
            }
            setPageInfo(page_info)
        }).catch(console.log)
    }

    const onRefresh = () => {
        getImagesFromLibrary(true)
    }

    const onLoadMore = () => {
        if (!pageInfo.has_next_page) {
            console.log('no more')
            return
        }
        getImagesFromLibrary(false)

    }

    const onPress = item => {
        setImageInfo({
            name: item.node.image.filename,
            width: item.node.image.width,
            height: item.node.image.height,
            uri: item.node.image.uri,
            location: JSON.stringify(item.node.location),
            timestamp: item.node.timestamp
        })
    }



    return (
        <SafeAreaView style={styles.container}>
            <StatusBar barStyle="dark-content"/>
            <View style={styles.infoContainer}>
                <View style={styles.infoRow}>
                    <Text>name:</Text>
                    <Text>{imageInfo.name}</Text>
                </View>
                <View style={styles.infoRow}>
                    <Text>width: {imageInfo.width} - height: {imageInfo.height}</Text>
                </View>
                <View style={styles.infoRow}>
                    <Text>timestamp:</Text>
                    <Text>{moment.unix(imageInfo.timestamp).format('HH:mm MM/DD/YYYY')}</Text>
                </View>
                <View style={styles.infoRow}>
                    <Text>uri:</Text>
                    <Text>{imageInfo.uri}</Text>
                </View>
                <View style={styles.infoRow}>
                    <Text>{imageInfo.location}</Text>
                </View>
                {isEmpty(imageInfo) ? null
                : <View style={styles.infoRow}>
                    <View style={{flex: 1}}/>
                    <TouchableOpacity
                        style={{
                            backgroundColor: '#f06060',
                            paddingHorizontal: 8,
                            paddingVertical: 4,
                            borderRadius: 4
                        }}
                        onPress={() => {
                            navigate('Detail', {
                                item: imageInfo,
                                onComplete: onRefresh
                            })
                        }}>
                        <Text style={{ color: 'white' }}>Detail</Text>
                    </TouchableOpacity>
                </View> }
            </View>
            <FlatList
                contentInsetAdjustmentBehavior="automatic"
                style={styles.scrollView}
                data={images}
                numColumns={3}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh} />
                }
                onEndReachedThreshold={0.4}
                onEndReached={onLoadMore}
                keyExtractor={(item, index) => `_${index}`}
                renderItem={({item}) => {
                    return (
                        <TouchableOpacity onPress={() => onPress(item)}>
                            <Image style={styles.image} source={{uri: item.node.image.uri}}/>
                        </TouchableOpacity>
                    )}}
            />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    scrollView: {
        flex: 1,
        backgroundColor: Colors.white,
    },
    contentScroll: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingVertical: 16
    },
    infoContainer: {
        backgroundColor: Colors.white,
    },
    infoRow: {
        flexDirection: 'row',
        paddingVertical: 4,
        paddingHorizontal: 16,
    },
    image: {
        resizeMode: 'contain',
        width: Dimensions.get('window').width / 3 - 4,
        height: Dimensions.get('window').width / 3 - 4,
        marginHorizontal: 2,
        marginVertical: 2,
        backgroundColor: Colors.lighter
    }

});

export default Home;
