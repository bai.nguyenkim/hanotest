import React from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";
import Home from './src/screens/home'
import Detail from './src/screens/detail'

const AppNavigator = createStackNavigator({
    Home: {
        screen: Home
    },
    Detail: {
        screen: Detail
    }
}, {
    headerMode: 'none'
});

export default createAppContainer(AppNavigator);