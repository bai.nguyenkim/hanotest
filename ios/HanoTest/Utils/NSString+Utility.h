//
//  NSString+Utility.h
//  HanoTest
//
//  Created by kimbai on 8/4/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utilities)

+ (NSString *)randomStringWithLength:(int)length;

@end
