//
//  NSString+Utility.m
//  HanoTest
//
//  Created by kimbai on 8/4/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "NSString+Utility.h"

@implementation NSString (Utilities)

+ (NSString *)randomStringWithLength:(int)length {
  NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  NSMutableString *randomString = [NSMutableString stringWithCapacity:length];
  for (int i = 0; i < length; i++) {
    [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
  }
  return randomString;
}

@end
