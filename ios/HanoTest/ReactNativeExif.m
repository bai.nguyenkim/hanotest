#import "ReactNativeExif.h"
#import <React/RCTBridge.h>
#import <React/RCTEventDispatcher.h>
#import <React/RCTLog.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "NSString+Utility.h"
#import "NSDictionary+JSON.h"
#import "PHAsset+Utility.h"


@import Photos;

@implementation ReactNativeExif

RCT_EXPORT_MODULE(ReactNativeExif)

RCT_EXPORT_METHOD(getMetaData:(NSString *)path resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
  NSString *localIdentifier;
  if ([path hasPrefix:@"ph://"]) {
    localIdentifier = [path stringByReplacingOccurrencesOfString:@"ph://" withString:@""];
  }
  if (!localIdentifier) {
    reject(@"no_asset", @"There were no asset with this local identifier", [NSError errorWithDomain:NSURLErrorDomain code:1 userInfo:nil]);
    return;
  }
  PHAsset *asset = [self fetchAssetsWithLocalIdentifier:localIdentifier];
  if (!asset) {
    reject(@"no_asset", @"There were no asset with this local identifier", [NSError errorWithDomain:NSURLErrorDomain code:1 userInfo:nil]);
    return;
  }
  
  [asset requestMetadataWithCompletionBlock:^(NSDictionary *metadata) {
    resolve(@[metadata]);
  }];
}

RCT_EXPORT_METHOD(cloneWithCurrentLocation:(NSString *)path position:(NSDictionary *)position resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
  NSString *localIdentifier;
  if ([path hasPrefix:@"ph://"]) {
    localIdentifier = [path stringByReplacingOccurrencesOfString:@"ph://" withString:@""];
  }
  if (!localIdentifier) {
    reject(@"no_asset", @"There were no asset with this local identifier", [NSError errorWithDomain:NSURLErrorDomain code:1 userInfo:nil]);
    return;
  }
  PHAsset *asset = [self fetchAssetsWithLocalIdentifier:localIdentifier];
  if (!asset) {
    reject(@"no_asset", @"There were no asset with this local identifier", [NSError errorWithDomain:NSURLErrorDomain code:1 userInfo:nil]);
    return;
  }
  
  [self requestImageDataForAsset:asset onComplete:^(NSData *imageData, NSString *UTI, UIImageOrientation orientation, NSDictionary *info) {
    UIImage *newImage = [UIImage imageWithData:imageData];
    CLLocationDegrees latitude = [position[@"coords"][@"latitude"] doubleValue];
    CLLocationDegrees longitude = [position[@"coords"][@"longitude"] doubleValue];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    [PHAsset saveImageToCameraRoll:newImage location:location completionBlock:^(PHAsset *asset, BOOL success) {
      if (success) {
        resolve(@[]);
      } else {
        reject(@"asset_save_fail", @"Cannot save this image to camera roll", [NSError errorWithDomain:NSCocoaErrorDomain code:1 userInfo:nil]);
      }
    }];
  }];
}

RCT_EXPORT_METHOD(cloneImage:(NSString *)path position:(NSDictionary *)position resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {
  NSString *localIdentifier;
  if ([path hasPrefix:@"ph://"]) {
    localIdentifier = [path stringByReplacingOccurrencesOfString:@"ph://" withString:@""];
  }
  if (!localIdentifier) {
    return;
  }
  PHAsset *asset = [self fetchAssetsWithLocalIdentifier:localIdentifier];
  if (!asset) {
    return;
  }
  
  [self requestImageDataForAsset:asset onComplete:^(NSData *imageData, NSString *UTI, UIImageOrientation orientation, NSDictionary *info) {
    CGImageSourceRef imgSource = CGImageSourceCreateWithData((__bridge_retained CFDataRef)imageData, NULL);
    
    //get all the metadata in the image
    NSDictionary *metadata = (__bridge NSDictionary *)CGImageSourceCopyPropertiesAtIndex(imgSource, 0, NULL);
    
    //make the new metadata dictionary mutable
    NSMutableDictionary *metadataAsMutable = [self makeMetaDataFromCurrent:metadata];
    
    //this will be the data CGImageDestinationRef will write into
    NSMutableData *newImageData = [NSMutableData data];
    CGImageDestinationRef destination = CGImageDestinationCreateWithData((__bridge CFMutableDataRef)newImageData, (__bridge CFStringRef)UTI, 1, NULL);
    
    if(!destination)
      NSLog(@"***Could not create image destination ***");
    
    //add the image contained in the image source to the destination, overidding the old metadata with our modified metadata
    CGImageDestinationAddImageFromSource(destination, imgSource, 0, (__bridge CFDictionaryRef) metadataAsMutable);
    
    //tell the destination to write the image data and metadata into our data object.
    //It will return false if something goes wrong
    BOOL success = NO;
    success = CGImageDestinationFinalize(destination);
    
    if(!success)
      NSLog(@"***Could not create data from image destination ***");
    
    //now we have the data ready to go, so do whatever you want with it
    //here we just write it to disk at the same path we were passed

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"ImagesFolder"];

    NSError *error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
      [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    NSString *fullPath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", [self makeNewFileName]]]; //add our image to the path
    [newImageData writeToFile:fullPath atomically:YES];
    
    //cleanup
    
    CFRelease(destination);
    CFRelease(imgSource);
    
    NSLog(@"info: %@", info);
    
    CLLocationDegrees latitude = [position[@"coords"][@"latitude"] doubleValue];
    CLLocationDegrees longitude = [position[@"coords"][@"longitude"] doubleValue];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
      PHAssetChangeRequest *assetRequest = [PHAssetChangeRequest creationRequestForAssetFromImageAtFileURL:[NSURL URLWithString:fullPath]];
      assetRequest.location = location;
      assetRequest.creationDate = [NSDate date];
    } completionHandler:^(BOOL success, NSError *error) {
      if (success){
        NSLog(@"success");
        [[NSFileManager defaultManager] removeItemAtPath:fullPath error:nil];
        resolve(@[]);
      }else{
        NSLog(@"%@",error.description);
      }
    }];
  }];
}

- (PHAsset *)fetchAssetsWithLocalIdentifier:(NSString *)localIdentifier {
  PHFetchResult* fetchResult = [PHAsset fetchAssetsWithLocalIdentifiers:@[localIdentifier] options:nil];
  if (fetchResult.count > 0) {
    return [fetchResult objectAtIndex:0];
  }
  return nil;
}

- (void)requestImageDataForAsset:(PHAsset *)asset onComplete:(void(^)(NSData *, NSString *, UIImageOrientation, NSDictionary*))onComplete {
  PHImageManager *manager = [PHImageManager defaultManager];
  PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
  options.version = PHImageRequestOptionsVersionOriginal;
  [options setSynchronous:YES];
  [manager requestImageDataForAsset:asset options:options resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
    onComplete(imageData, dataUTI, orientation, info);
  }];
}

- (NSMutableDictionary *)makeMetaDataFromCurrent:(NSDictionary *)current {
  NSMutableDictionary *metaData = [current mutableCopy];
  
  NSMutableDictionary *EXIFDictionary = [[metaData objectForKey:(NSString *)kCGImagePropertyExifDictionary]mutableCopy];
  NSMutableDictionary *GPSDictionary = [[metaData objectForKey:(NSString *)kCGImagePropertyGPSDictionary]mutableCopy];
  NSMutableDictionary *RAWDictionary = [[metaData objectForKey:(NSString *)kCGImagePropertyRawDictionary]mutableCopy];
  
  if(!EXIFDictionary)
    EXIFDictionary = [[NSMutableDictionary dictionary] init];
  
  if(!GPSDictionary)
    GPSDictionary = [[NSMutableDictionary dictionary] init];
  
  if(!RAWDictionary)
    RAWDictionary = [[NSMutableDictionary dictionary] init];
  
  
  [GPSDictionary setObject:[NSNumber numberWithFloat:37.795]
                    forKey:(NSString*)kCGImagePropertyGPSLatitude];
  
  [GPSDictionary setObject:@"N" forKey:(NSString*)kCGImagePropertyGPSLatitudeRef];
  
  [GPSDictionary setObject:[NSNumber numberWithFloat:122.410]
                    forKey:(NSString*)kCGImagePropertyGPSLongitude];
  
  [GPSDictionary setObject:@"W" forKey:(NSString*)kCGImagePropertyGPSLongitudeRef];
  
  [GPSDictionary setObject:@"2012:10:18"
                    forKey:(NSString*)kCGImagePropertyGPSDateStamp];
  
  [GPSDictionary setObject:[NSNumber numberWithFloat:300]
                    forKey:(NSString*)kCGImagePropertyGPSImgDirection];
  
  [GPSDictionary setObject:[NSNumber numberWithFloat:37.795]
                    forKey:(NSString*)kCGImagePropertyGPSDestLatitude];
  
  [GPSDictionary setObject:@"N" forKey:(NSString*)kCGImagePropertyGPSDestLatitudeRef];
  
  [GPSDictionary setObject:[NSNumber numberWithFloat:122.410]
                    forKey:(NSString*)kCGImagePropertyGPSDestLongitude];
  
  [GPSDictionary setObject:@"W" forKey:(NSString*)kCGImagePropertyGPSDestLongitudeRef];
  
  [EXIFDictionary setObject:@"[S.D.] kCGImagePropertyExifUserComment"
                     forKey:(NSString *)kCGImagePropertyExifUserComment];
  
  [EXIFDictionary setObject:[NSNumber numberWithFloat:69.999]
                     forKey:(NSString*)kCGImagePropertyExifSubjectDistance];
  
  
  //Add the modified Data back into the image’s metadata
  [metaData setObject:EXIFDictionary forKey:(NSString *)kCGImagePropertyExifDictionary];
  [metaData setObject:GPSDictionary forKey:(NSString *)kCGImagePropertyGPSDictionary];
//  [metaData setObject:RAWDictionary forKey:(NSString *)kCGImagePropertyRawDictionary];
  
  return metaData;
}

- (NSString *)makeNewFileName {
  NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
  [formatter setDateFormat:@"YYYYMMDDHHmmss"];
  [formatter setTimeZone:[NSTimeZone localTimeZone]];
  
  return [NSString stringWithFormat:@"%@-%@", [formatter stringFromDate:[NSDate date]], [NSString randomStringWithLength:6]];
  
}

@end
